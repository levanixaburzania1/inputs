import { useState } from "react";
import Check from './Check';
import Show from './Show'

const Inputs = () => {
  

  const [inputValues,setInputValues] = useState({
    textArea:"",
    textInput:""
  });

  const textAreaaInputChange = (event) => {
    setInputValues({
      ...inputValues,
     textArea: event.target.value,
     
    })
  }

  const textInputChange = (event) => {
    setInputValues({
      ...inputValues,
     textInput: event.target.value,
     
    })
  }
const [selectValue, setSelectValue] = useState("");

const selectChanged = (event) => {
  setSelectValue(event.target.value)
}


  return (
    <div>
      <textarea
      placeholder="enter text in textarea"
        id="w3review"
        name="w3review"
        rows="4"
        cols="50"
        onChange={textAreaaInputChange}
      >
        {inputValues.textArea}
      </textarea>
      <button onClick={() =>alert(inputValues.textArea)}>click</button>
      <br/>
      <input type="text" placeholder="type text" value={inputValues.textInput} onChange={textInputChange}></input>
      <button onClick={() =>alert(inputValues.textInput)}>click</button>
      <br/>
      <select onChange={selectChanged}>
        <option>10</option>
        <option>12</option>
        <option>13</option>
        <option>14</option>
      </select>
      <button onClick={()=>alert(selectValue)}>click</button>
      <br/>
<Check selected title="pineapple"/>
<Check title="pen"/>
<Check title="apple"/>
<Show/>
    </div>
  );
};

export default Inputs;
